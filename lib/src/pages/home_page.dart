import 'package:flutter/material.dart';

//CASI TODOS LOS WIDGET PUEDEN CONTENER UN HIJO
class HomePage extends StatelessWidget {
  final TextStyle estiloTexto = new TextStyle(fontSize: 30);
  final Color colorIcon = new Color(0xFF263238);

  final int conteo = 10;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Título'),
        centerTitle: true,
        backgroundColor: Color(0xFF448AFF),
        elevation: 8.0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Numero de clicks:', style: estiloTexto),
            Text('$conteo', style: estiloTexto)
          ],
        ),
      ),
      //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat, -> CAMBIAR LA UBICACION
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print("HOLA AL CLICK");
        }, // () {} funcion anonima
        backgroundColor: Color(0xFF2979FF),
        foregroundColor: Color(0xFF82B1FF),
        isExtended: true,
        child: Icon(Icons.add, color: colorIcon),
      ),
    );
  }
}
