import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  @override
  createState() => _ContadorPageState();
}

// EL StatefulWidget NECESITA AFUERZAS UN METODO ES EL SIGUIENTE:

class _ContadorPageState extends State<ContadorPage> {
  final _estiloTexto = new TextStyle(fontSize: 30);
  final _colorIcon = new Color(0xFFFAFAFA);

  int _conteo = 0;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Stateful'),
          centerTitle: true,
          backgroundColor: Color(0xFF448AFF),
          elevation: 8.0,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Numero de taps:', style: _estiloTexto),
              Text('$_conteo', style: _estiloTexto)
            ],
          ),
        ),
        //floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat, -> CAMBIAR LA UBICACION
        floatingActionButton: _crearBotones());
  }

  Widget _crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        SizedBox(width: 30.0),
        // CERO
        FloatingActionButton(
          onPressed: _reset, // () {} funcion anonima
          backgroundColor: Color(0xFF82B1FF),
          foregroundColor: Color(0xFF82B1FF),
          isExtended: true,
          child: Icon(Icons.exposure_zero, color: _colorIcon),
        ),
        Expanded(child: SizedBox()),
        // DECREMENTO
        FloatingActionButton(
          onPressed: _sustraer, // () {} funcion anonima
          backgroundColor: Color(0xFFFF1744),
          foregroundColor: Color(0xFFFF5252),
          isExtended: true,
          child: Icon(Icons.remove, color: _colorIcon),
        ),
        SizedBox(width: 5.0),
        //INCREMENTO
        FloatingActionButton(
          onPressed: _agregar, // () {} funcion anonima
          backgroundColor: Color(0xFF2979FF),
          foregroundColor: Color(0xFF82B1FF),
          isExtended: true,
          child: Icon(Icons.add, color: _colorIcon),
        ),
      ],
    );
  }

  //void porque no rgresa nada
  void _agregar() {
    setState(() => _conteo++);
  }

  void _sustraer() {
    setState(() {
      _conteo--;
      if (_conteo < 0) {
        _conteo = 0;
      }
    });
  }

  void _reset() {
    setState(() => _conteo = 0);
  }
}
